-- RPMDisplayControl -> RPMDisplayControl
-- Specialisation for RPMDisplayControl / RPMDisplayControl
--
-- @ Autor  Tobias F. (John Deere 6930) / fruktor
-- @ Last Edit  04/08/2011 / 27/08/2011
--

RPMDisplayControl = {};

function RPMDisplayControl.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(Motorized, specializations);
end;

function RPMDisplayControl:load(xmlFile)
	self.getNumbersPerFloatRPM = RPMDisplayControl.getNumbersPerFloatRPM;
	self.updateRPMDisplay = RPMDisplayControl.updateRPMDisplay;
	self.rpmDisplayEntries = {};
    local i = 0;
    while true do
		local key = string.format("vehicle.RPMDisplayEntries.displayEntry(%d)", i);
		local control_0 = getXMLString(xmlFile, key.."#control_0");
		local control_1 = getXMLString(xmlFile, key.."#control_1");
		local control_2 = getXMLString(xmlFile, key.."#control_2");
		local control_3 = getXMLString(xmlFile, key.."#control_3");
		local control_4 = getXMLString(xmlFile, key.."#control_4");
		local control_5 = getXMLString(xmlFile, key.."#control_5");
		local control_6 = getXMLString(xmlFile, key.."#control_6");
		local control_7 = getXMLString(xmlFile, key.."#control_7");
		local control_8 = getXMLString(xmlFile, key.."#control_8");
		local control_9 = getXMLString(xmlFile, key.."#control_9");
		local displayEntry = getXMLString(xmlFile, key.."#displayEntry");
		if (control_0 and control_1 and control_2 and control_3 and control_4 and control_5 and control_6 and control_7 and control_8 and control_9 and displayEntry)~= nil and displayEntry ~= "" then
			local entry = {};
			entry.control = {};
			entry.control[0] = Utils.indexToObject(self.components, control_0);
			entry.control[1] = Utils.indexToObject(self.components, control_1);
			entry.control[2] = Utils.indexToObject(self.components, control_2);
			entry.control[3] = Utils.indexToObject(self.components, control_3);
			entry.control[4] = Utils.indexToObject(self.components, control_4);
			entry.control[5] = Utils.indexToObject(self.components, control_5);
			entry.control[6] = Utils.indexToObject(self.components, control_6);
			entry.control[7] = Utils.indexToObject(self.components, control_7);
			entry.control[8] = Utils.indexToObject(self.components, control_8);
			entry.control[9] = Utils.indexToObject(self.components, control_9);
			entry.displayEntry = displayEntry;
			table.insert(self.rpmDisplayEntries, entry);
		else
			break;
		end;
		i = i + 1;
	end;
end;

function RPMDisplayControl:delete()
end;

function RPMDisplayControl:mouseEvent(posX, posY, isDown, isUp, button)
end;

function RPMDisplayControl:keyEvent(unicode, sym, modifier, isDown)
end;

function RPMDisplayControl:update(dt)
end;

function RPMDisplayControl:getNumbersPerFloatRPM(floatNumber)
	local numbers = {0, 0, 0, 0};
	numbers[4] = math.floor( floatNumber / 1000 );
	numbers[3] = math.max(0, math.floor( (floatNumber - numbers[4]*1000) / 100 ) );
	numbers[2] = math.max(0, math.floor( (floatNumber - numbers[4]*1000 - numbers[3]*100) / 10 ) );
	numbers[1] = math.max(0, math.floor( (floatNumber - numbers[4]*1000 - numbers[3]*100 - numbers[2]*10) ) );
	return numbers;
end;

function RPMDisplayControl:updateTick(dt)
    if self:getIsActive() then
		if self.isMotorStarted then
			
			local lastRpm = math.max(0, self.motor.lastMotorRpm);
			local rpmTable = self:getNumbersPerFloatRPM(lastRpm)
			local n = table.getn(rpmTable)
			local numberCell = {};
			if n == 4 then
				numberCell["first"] = rpmTable[1];
				numberCell["second"] = rpmTable[2];
				numberCell["third"] = rpmTable[3];
				numberCell["fourth"] = rpmTable[4];
			end;			
			for k, entry in pairs(self.rpmDisplayEntries) do
				self:updateRPMDisplay(entry, numberCell[entry.displayEntry]);
			end;			
		end;
	end;
end;

function RPMDisplayControl:updateRPMDisplay(entry, index)
	for k, control in pairs(entry.control) do
		setVisibility(control, false);
		if index == 0 or index == nil then
			setVisibility(entry.control[0], true);
		else
			if k == index then
				setVisibility(control, true);
			end;
		end;
	end;
end;

function RPMDisplayControl:draw()
end;