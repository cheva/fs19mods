-- SpeedDisplayControl
-- Specialisation for SpeedDisplayControl
--
-- @ Autor  Tobias F. (John Deere 6930) 
-- @ Last Edit  04/08/2011

SpeedDisplayControl = {};

function SpeedDisplayControl.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(Motorized, specializations);
end;

function SpeedDisplayControl:load(xmlFile)
	self.getNumbersPerFloat = SpeedDisplayControl.getNumbersPerFloat;
	self.updateSpeedDisplay = SpeedDisplayControl.updateSpeedDisplay;
	self.speedDisplayEntries = {};
    local i = 0;
    while true do
		local key = string.format("vehicle.speedDisplayEntries.displayEntry(%d)", i);
		local control_0 = getXMLString(xmlFile, key.."#control_0");
		local control_1 = getXMLString(xmlFile, key.."#control_1");
		local control_2 = getXMLString(xmlFile, key.."#control_2");
		local control_3 = getXMLString(xmlFile, key.."#control_3");
		local control_4 = getXMLString(xmlFile, key.."#control_4");
		local control_5 = getXMLString(xmlFile, key.."#control_5");
		local control_6 = getXMLString(xmlFile, key.."#control_6");
		local control_7 = getXMLString(xmlFile, key.."#control_7");
		local control_8 = getXMLString(xmlFile, key.."#control_8");
		local control_9 = getXMLString(xmlFile, key.."#control_9");
		local displayEntry = getXMLString(xmlFile, key.."#displayEntry");
		if (control_0 and control_1 and control_2 and control_3 and control_4 and control_5 and control_6 and control_7 and control_8 and control_9 and displayEntry)~= nil and displayEntry ~= "" then
			local entry = {};
			entry.control = {};
			entry.control[0] = Utils.indexToObject(self.components, control_0);
			entry.control[1] = Utils.indexToObject(self.components, control_1);
			entry.control[2] = Utils.indexToObject(self.components, control_2);
			entry.control[3] = Utils.indexToObject(self.components, control_3);
			entry.control[4] = Utils.indexToObject(self.components, control_4);
			entry.control[5] = Utils.indexToObject(self.components, control_5);
			entry.control[6] = Utils.indexToObject(self.components, control_6);
			entry.control[7] = Utils.indexToObject(self.components, control_7);
			entry.control[8] = Utils.indexToObject(self.components, control_8);
			entry.control[9] = Utils.indexToObject(self.components, control_9);
			entry.displayEntry = displayEntry;
			table.insert(self.speedDisplayEntries, entry);
		else
			break;
		end;
		i = i + 1;
	end;
end;

function SpeedDisplayControl:delete()
end;

function SpeedDisplayControl:mouseEvent(posX, posY, isDown, isUp, button)
end;

function SpeedDisplayControl:keyEvent(unicode, sym, modifier, isDown)
end;

function SpeedDisplayControl:update(dt)
end;

function SpeedDisplayControl:getNumbersPerFloat(floatNumber)
	local numbers = {};
	local intNumber = math.floor(floatNumber*100);
	
	repeat
		local modulo = intNumber % 10;
		table.insert(numbers, 1, modulo);
		intNumber = (intNumber - modulo) / 10;
	until intNumber == 0;

	return numbers;
end;

function SpeedDisplayControl:updateTick(dt)
    if self:getIsActive() then
		if self.isMotorStarted then
			-----------------------------
			--[[kmh = math.floor(kmh*100)/100;
			kmh = tostring(kmh);
			local finKmhTable = {};
			
			local parts = Utils.splitString(".", kmh);
			local len = string.len(parts[2]);
			if len < 1 then
				finKmhTable["comma_second"] = tonumber(string.sub(parts[2], 2, 2)); -- aaa,ab
				if finKmhTable["comma_second"] ~= nil then
					print("comma_second "..finKmhTable["comma_second"])
				end;
			end;
			if len == 1 then
				finKmhTable["comma_first"] = tonumber(string.sub(parts[2], 1, 1)); -- aaa,ba
				if finKmhTable["comma_first"] ~= nil then
					print("comma_first "..finKmhTable["comma_first"])
				end;
			end;
			len = string.len(parts[1]);
			finKmhTable["first"] = tonumber(string.sub(parts[1], len, len)); -- aab,aa
				if finKmhTable["first"] ~= nil then
					print("first "..finKmhTable["first"])
				end;
			if len-1 >= 1 then
				finKmhTable["second"] = tonumber(string.sub(parts[1], len-1, len)); -- aba,aa
				if finKmhTable["second"] ~= nil then
					print("second "..finKmhTable["second"])
				end;
			end;
			if len-2 >= 1 then
				finKmhTable["third"] = tonumber(string.sub(parts[1], len-2, len)); -- baa,aa
				if finKmhTable["third"] ~= nil then
					print("third "..finKmhTable["third"])
				end;
			end;
			
			
			local kmhTable = {};
			while math.floor(kmh) ~= kmh do
				kmh = kmh * 10;
			end;
			while kmh > 0 do
				local ziffer = kmh % 10;
				table.insert(kmhTable, ziffer);
				kmh = math.floor(kmh / 10);
			end;
			
			local printstring = "";
			
			local n = table.getn(kmhTable);
			for i=0, n do
				w = n-i;				
				if n-5 < w then
					break;
				else
					table.insert(finKmhTable,kmhTable[w])
					printstring = printstring..kmhTable[w];
				end;
			end;
			for k, entry in pairs(self.speedDisplayEntries) do
				if entry.displayEntry == "first" then
					self:updateSpeedDisplay(entry, firstNumber);
				elseif entry.displayEntry == "second" then
					self:updateSpeedDisplay(entry, secondNumber);
				elseif entry.displayEntry == "third" then
					self:updateSpeedDisplay(entry, kmhTable[5]);
				elseif entry.displayEntry == "comma_first" then
					self:updateSpeedDisplay(entry, kmhTable[2]);
				elseif entry.displayEntry == "comma_second" then
					self:updateSpeedDisplay(entry, kmhTable[1]);
				end;
			end;			
			
			print(tostring(printstring));]]
			-----------------------------
			
			local kmh = math.max(0, self.lastSpeed*self.speedDisplayScale*3600);
			local kmhTable = self:getNumbersPerFloat(kmh)
			local n = table.getn(kmhTable)
			
			local numberCell = {};
			if n == 3 then
				numberCell["first"] = kmhTable[1];
				numberCell["second"] = 0;
				numberCell["third"] = 0;
				numberCell["comma_first"] = kmhTable[2];
				numberCell["comma_second"] = kmhTable[3];
			elseif n == 4 then
				numberCell["first"] = kmhTable[2];
				numberCell["second"] = kmhTable[1];
				numberCell["third"] = 0;
				numberCell["comma_first"] = kmhTable[3];
				numberCell["comma_second"] = kmhTable[4];
			elseif n == 5 then
				numberCell["first"] = kmhTable[3];
				numberCell["second"] = kmhTable[2];
				numberCell["third"] = kmhTable[1];
				numberCell["comma_first"] = kmhTable[4];
				numberCell["comma_second"] = kmhTable[5];
			end;			
			for k, entry in pairs(self.speedDisplayEntries) do
				self:updateSpeedDisplay(entry, numberCell[entry.displayEntry]);
			end;			
		end;
	end;
end;

function SpeedDisplayControl:updateSpeedDisplay(entry, index)
	for k, control in pairs(entry.control) do
		setVisibility(control, false);
		if index == 0 or index == nil then
			setVisibility(entry.control[0], true);
		else
			if k == index then
				setVisibility(control, true);
			end;
		end;
	end;
end;

function SpeedDisplayControl:draw()
end;