# Farming Simulator mods by Cheva

Current versions: x.0.5R

	- Agrisem Cultiplow 8m with field creation function
	- Bizon Z056 Combine pack with cutters (NEW not tested: sugar cane cutter), 
		with new engine, colors, lights, SHADOWS and sound
	- Bulk Silo 500k m3 from american map models
	- CanAm Outlander (POPULAR)
	- Challenger MT800E with new chassics
	- Fastrac 4220 able to use Agrisem, with new engine, colors, lights and sound
	- Fendt Favorit 500 with new engine, colors, lights and sound
	- Fertilizer Pack: 
		* K105 and K165 with new volumes, narrow wheels and lime press function, 
		* Axis 402 with new volumes
		* Axis 402S (NEW) seeder alpha (without animation yet), 
		* Metris 4102 with narrow wheels
	- Fiat 1300DT with new engine, colors, lights and sound
	- Grrimme GS860 Compacta NoTill Multifruit able to plant sugar cane
	- Horsch Pronto 9DC NoTill Multifruit
	- John Deere 7R with new engine, colors, lights and sound
	- Joker 12 RT
	- Kinze Wagon Multifruit
	- Krone BiGM 450 able to use Kuhn balers, with new engine, colors, lights and sound
	- Kuhn FBP3135 baler with new volumes, colors alarm sounds and Krone steerling option
	- Kuhn LSB 1290D baler with new volumes and Krone steerling option
	- multi Attacher Foldable able to attach up to 3 implements 
		(for example 2 Agrisem plows or Pronto 9DC)
	- New Holland FR780 with new engine, colors, lights and sound
	- ReFill Station to buy anything that you can sell on the map
	- RSM 161 alpha without cutters with new volumes, colors, lights and sound
	- Selling Stations to sell anything that you can sell on map, 5 varioations, 
		different prices
	- TAW 30 Multifruit
	- Trailer Pack Multifruit
		* DK160, 
		* TDK160
		* SEK802 
		with many volume options and narrow wheels
	- yYield alpha version - brand new seasons manager for basic 10 fruits and 4 stages: 
		* Pre-Season: Higher growth and price, 
		* SEASON: Higher yield, 
		* Post-Season: Lower price and growth, high yield, 
		* Off-Season: Normal price, growth and yield
___
github | https://github.com/dcheva  
fcbook | https://www.facebook.com/groups/FS19modsByCheva/  
photos | https://photos.app.goo.gl/yGuKWuhQpsGqP2s79  
yutube | https://www.youtube.com/user/dmitrycheva  